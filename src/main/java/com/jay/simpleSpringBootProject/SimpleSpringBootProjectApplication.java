package com.jay.simpleSpringBootProject;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class SimpleSpringBootProjectApplication implements CommandLineRunner{

	public static void main(String[] args) {
		SpringApplication.run(SimpleSpringBootProjectApplication.class, args);
	}
	
	@Override
	public void run(String... args) throws Exception {
		System.out.println("ClmBatchJob RUN!!! RUN!!! for Java 1.7+ ");
		System.out.println("#################################");
		System.out.println("#################################");
		System.out.println("#################################");
	}
}
